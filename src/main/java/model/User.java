package model;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "users")
public class User {
    private Long id;
    private String name;
    private String lastname;
    private String password;
    private String email;
    private String lastActive;
    private Long projectId;
    private Project project;
    private List<Role> roleList;
    private List<Task> taskList;

    public User() {
    }

    @Id
    @GeneratedValue
    @Column(name = "id")
    public Long getId() {
        return id;
    }

    @Column(name = "name")
    public String getName() {
        return name;
    }

    @Column(name = "lastname")
    public String getLastname() {
        return lastname;
    }

    @Column(name = "password")
    public String getPassword() {
        return password;
    }

    @Column(name = "email")
    public String getEmail() {
        return email;
    }

    @Column(name = "last_active")
    public String getLastActive() {
        return lastActive;
    }

    @Column(name ="project_id")
    public Long getProjectId() {
        return projectId;
    }
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "project_id")
    public Project getProject() {
        return project;
    }

    @ManyToMany(mappedBy = "userList")
    public List<Role> getRoleList() {
        return roleList;
    }
    @ManyToMany(mappedBy = "userList")
    public List<Task> getTaskList() {
        return taskList;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setLastActive(String lastActive) {
        this.lastActive = lastActive;
    }

    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public void setRoleList(List<Role> roleList) {
        this.roleList = roleList;
    }

    public void setTaskList(List<Task> taskList) {
        this.taskList = taskList;
    }
}
