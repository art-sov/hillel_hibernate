package model;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "tasks")
public class Task {
    private Long id;
    private String name;
    private String status;
    private String date;
    private Long progectId;
    private Project project;
    private List<User> userList;

    public Task() {
    }

    @Id
    @GeneratedValue
    @Column(name = "id")
    public Long getId() {
        return id;
    }
    @Column(name = "name")
    public String getName() {
        return name;
    }

    @Column(name = "status")
    public String getStatus() {
        return status;
    }

    @Column(name = "date")
    public String getDate() {
        return date;
    }

    @Column(name = "project_id")
    public Long getProgectId() {
        return progectId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "project_id")
    public Project getProject() {
        return project;
    }

    @ManyToMany
    @JoinTable(name = "user_task",
            joinColumns = @JoinColumn(name = "task_id"),
            inverseJoinColumns = @JoinColumn(name = "user_id"))
    public List<User> getUserList() {
        return userList;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setProgectId(Long progect_id) {
        this.progectId = progect_id;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public void setUserList(List<User> userList) {
        this.userList = userList;
    }
}
