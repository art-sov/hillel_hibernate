import model.*;
import org.hibernate.Session;
import util.HibernateUtil;

public class Main {

    public static void main(String[] args) {
        Session session = HibernateUtil.getSession();
        session.beginTransaction();

        User user = (User) session.get(User.class, 1L);
        System.out.println(user.getName());

        Task task = (Task) session.get(Task.class, 2L);
        System.out.println(task.getName());

        Comment comment = (Comment) session.get(Comment.class, 2L);
        System.out.println(comment.getContent());

        Role role = (Role) session.get(Role.class, 2L);
        System.out.println(role.getDescription());

        Project project = (Project) session.get(Project.class, 2l);
        System.out.println(project.getName());

        session.getTransaction().commit();
        session.close();
        HibernateUtil.shutdown();
    }
}
